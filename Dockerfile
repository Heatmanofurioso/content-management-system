FROM maven:3.8.2-openjdk-17 AS build

WORKDIR /app

COPY pom.xml /app/pom.xml
RUN mvn dependency:go-offline

COPY src/ /app/src/
RUN mvn package


FROM openjdk:17-jdk-slim

WORKDIR /app

EXPOSE 8080
EXPOSE 9080
EXPOSE 4002
EXPOSE 443

COPY --from=build /app/src/main/resources/keystore/cacerts $JAVA_HOME/lib/security

ENTRYPOINT ["java", "-jar", "-Dfile.encoding=UTF8", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:4002", "cms.jar"]
COPY --from=build /app/target/*.jar /app/cms.jar
