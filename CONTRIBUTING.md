## Contributors ✨

Thanks goes to these wonderful people ([emoji key](https://allcontributors.org/docs/en/emoji-key)):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tr>
    <td><a href="https://gitlab.com/Heatmanofurioso"><img src="https://gitlab.com/uploads/-/system/user/avatar/747737/avatar.png?width=400?s=100" width="100px;" alt=""/><br /><sub><b>Tiago Casinhas</b></sub></a><br /><a href="https://gitlab.com/Heatmanofurioso" title="Code">💻</a> <a href="#business-heatmanofurioso" title="Business development">💼</a> <a href="#design-heatmanofurioso" title="Design">🎨</a> <a href="#mentoring-heatmanofurioso" title="Mentoring">🧑‍🏫</a> <a href="#projectManagement-heatmanofurioso" title="Project Management">📆</a> <a href="https://gitlab.com/Heatmanofurioso" title="Reviewed Pull Requests">👀</a> <a href="https://gitlab.com/Heatmanofurioso" title="Documentation">📖</a> <a href="#ideas-heatmanofurioso" title="Ideas, Planning, & Feedback">🤔</a> <a href="https://gitlab.com/Heatmanofurioso" title="Tests">⚠️</a></td>
  </tr>
</table>

<!-- markdownlint-restore -->
<!-- prettier-ignore-end -->

<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors](https://github.com/all-contributors/all-contributors) specification. Contributions of any kind welcome!
