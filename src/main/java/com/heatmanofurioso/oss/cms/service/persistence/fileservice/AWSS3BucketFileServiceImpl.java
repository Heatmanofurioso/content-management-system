package com.heatmanofurioso.oss.cms.service.persistence.fileservice;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.heatmanofurioso.oss.cms.service.persistence.IFileService;
import com.heatmanofurioso.oss.cms.utils.dto.entities.EntityTypes;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageType;
import com.heatmanofurioso.oss.cms.utils.exceptions.FileServiceException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Primary
@Profile("aws-s3")
@Slf4j
@Service
public class AWSS3BucketFileServiceImpl implements IFileService {

    private final AmazonS3 s3client;
    private final String rootFolderPath;
    private final String bucketName;

    @Autowired
    public AWSS3BucketFileServiceImpl(@Value("${cms.rootFolderPath}") String rootFolderPath,
                                      @Value("${cms.aws.bucket.name}") String bucketName,
                                      @Value("${cms.aws.bucket.region}") String bucketRegion,
                                      @Value("${cms.aws.bucket.accessKey}") String bucketAccessKey,
                                      @Value("${cms.aws.bucket.accessSecret}") String bucketSecret) {
        this.rootFolderPath = rootFolderPath;
        this.bucketName = bucketName;
        AWSCredentials credentials = new BasicAWSCredentials(
            bucketAccessKey,
            bucketSecret
        );

        s3client = AmazonS3ClientBuilder
            .standard()
            .withCredentials(new AWSStaticCredentialsProvider(credentials))
            .withRegion(Regions.fromName(bucketRegion))
            .build();

    }

    @Override
    public byte[] getDocumentContent(@NonNull String filePath) throws FileServiceException {
        S3Object s3object = s3client.getObject(bucketName, filePath);
        S3ObjectInputStream inputStream = s3object.getObjectContent();

        try {
            return IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            throw new FileServiceException(e);
        }
    }

    @Override
    public String saveDocumentContent(@NonNull EntityTypes entityType, @NonNull String entityValue, @NonNull String fileName, byte[] fileContent, @NonNull ImageType imageType)
    throws FileServiceException {

        String fileNameKey = rootFolderPath + "/" + imageType.name() + "/" + entityType.name();
        String fullFileName = fileNameKey + "/" + entityValue + "/" + fileName;

        File tempFile;
        try {
            tempFile = File.createTempFile(fileNameKey, entityValue, null);
        } catch (IOException e) {
            throw new FileServiceException(e);
        }
        try (FileOutputStream fos = new FileOutputStream(tempFile)) {
            fos.write(fileContent);
        } catch (IOException e) {
            throw new FileServiceException(e);
        }
        s3client.putObject(bucketName, fullFileName, tempFile);
        tempFile.delete();

        return fullFileName;
    }

    @Override
    public void deleteDocumentContent(@NonNull String documentPath) {
        s3client.deleteObject(bucketName, documentPath);
    }
}
