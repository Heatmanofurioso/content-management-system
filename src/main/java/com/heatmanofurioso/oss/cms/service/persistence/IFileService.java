package com.heatmanofurioso.oss.cms.service.persistence;

import com.heatmanofurioso.oss.cms.utils.dto.entities.EntityTypes;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageType;
import com.heatmanofurioso.oss.cms.utils.exceptions.FileServiceException;
import lombok.NonNull;

public interface IFileService {

    byte[] getDocumentContent(@NonNull String filePath) throws FileServiceException;

    String saveDocumentContent(@NonNull EntityTypes entityType, @NonNull String entityValue, @NonNull String fileName, byte[] fileContent,
                               @NonNull ImageType imageType) throws FileServiceException;

    void deleteDocumentContent(@NonNull String documentPath);
}
