package com.heatmanofurioso.oss.cms.service.persistence;

import com.heatmanofurioso.oss.cms.service.persistence.entity.ImageDocumentEntity;
import com.heatmanofurioso.oss.cms.service.persistence.repository.ImageDocumentRepository;
import com.heatmanofurioso.oss.cms.utils.dto.entities.EntityTypes;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageType;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Transactional
@Service
public class DocumentServiceImpl implements IDocumentService {
    private final ImageDocumentRepository documentRepository;

    @Autowired
    public DocumentServiceImpl(ImageDocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    @Override
    public ImageDocumentEntity create(@NonNull EntityTypes entityType, @NonNull String entityValue, @NonNull UUID documentGUID, @NonNull String documentPath, @NonNull String originalFileName,
                                      ImageType imageType) {
        ImageDocumentEntity documentEntity = new ImageDocumentEntity();
        documentEntity.setEntityType(entityType);
        documentEntity.setEntityValue(entityValue);
        documentEntity.setDocumentGUID(documentGUID);
        documentEntity.setDocumentPath(documentPath);
        documentEntity.setDocumentOriginalName(originalFileName);
        documentEntity.setImageType(imageType);
        return documentRepository.save(documentEntity);
    }

    @Override
    public ImageDocumentEntity findById(@NonNull UUID documentGUID) {
        return documentRepository.findDocumentByDocumentGUID(documentGUID);
    }

    @Override
    public List<ImageDocumentEntity> findAll() {
        return documentRepository.findAll();
    }

    @Override
    public void deleteById(@NonNull UUID guid) {
        documentRepository.deleteDocumentByDocumentGUID(guid);
    }

    @Override
    public List<ImageDocumentEntity> findAllByIdList(List<UUID> uuidList) {
        return documentRepository.findAllByDocumentGUIDIn(uuidList);
    }
}
