package com.heatmanofurioso.oss.cms.service.persistence;

import com.heatmanofurioso.oss.cms.service.persistence.entity.ImageDocumentEntity;
import com.heatmanofurioso.oss.cms.utils.dto.entities.EntityTypes;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageType;
import lombok.NonNull;

import java.util.List;
import java.util.UUID;

public interface IDocumentService {

    ImageDocumentEntity create(@NonNull EntityTypes entityType, @NonNull String entityValue, @NonNull UUID documentGUID, @NonNull String documentPath, @NonNull String originalFileName,
                               @NonNull ImageType imageType);

    ImageDocumentEntity findById(@NonNull UUID documentGUID);

    List<ImageDocumentEntity> findAll();

    void deleteById(@NonNull UUID guid);

    List<ImageDocumentEntity> findAllByIdList(List<UUID> uuidList);
}
