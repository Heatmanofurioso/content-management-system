package com.heatmanofurioso.oss.cms.service.persistence.fileservice;

import com.heatmanofurioso.oss.cms.service.persistence.IFileService;
import com.heatmanofurioso.oss.cms.utils.dto.entities.EntityTypes;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageType;
import com.heatmanofurioso.oss.cms.utils.exceptions.FileServiceException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Profile("!aws-prod")
@Slf4j
@Service
public class FileServiceImpl implements IFileService {

    private final String rootFolderPath;

    @Autowired
    public FileServiceImpl(@Value("${cms.rootFolderPath}") String rootFolderPath) {
        this.rootFolderPath = rootFolderPath;
    }

    @Override
    public byte[] getDocumentContent(@NonNull String filePath) throws FileServiceException {
        Path fullFilePath = Paths.get(rootFolderPath, filePath);
        File file = fullFilePath.toFile();

        if (!file.exists() || file.isDirectory() || !file.canRead()) {
            log.error("File does not exist or cannot be opened: {}", filePath);
            throw new FileServiceException(String.format("File does not exist or cannot be opened: %s", filePath));
        }

        try {
            return Files.readAllBytes(fullFilePath);
        } catch (IOException ex) {
            log.error(ex.getMessage());
            throw new FileServiceException(ex);
        }
    }

    @Override
    public String saveDocumentContent(@NonNull EntityTypes entityType, @NonNull String entityValue, @NonNull String fileName, byte[] fileContent,
                                      ImageType imageType) throws FileServiceException {
        Path directoryPath = Paths.get(rootFolderPath, imageType.name(), entityType.name(), entityValue);
        File directory = directoryPath.toFile();

        if (!directory.exists() && !directory.mkdirs()) {
            throw new FileServiceException("Failed to save document to disk");
        }

        Path filePath = directoryPath.resolve(fileName);
        File file = filePath.toFile();

        try (OutputStream os = new FileOutputStream(file)) {
            os.write(fileContent);
        } catch (IOException ex) {
            log.error("Failed to save document to disk");
            throw new FileServiceException(ex);
        }

        return Paths.get(rootFolderPath).relativize(filePath).toString();
    }

    @Override
    public void deleteDocumentContent(@NonNull String filePath) {
        File file = Paths.get(rootFolderPath, filePath).toFile();

        /*if (!file.exists() || file.isDirectory() || !file.canRead()) { //TODO - This if is commented out for now in dev
            log.error("File does not exist or cannot be opened: {}", filePath);
            throw new FileServiceException(String.format("File does not exist or cannot be opened: %s", filePath));
        }*/
        file.delete();
    }
}
