package com.heatmanofurioso.oss.cms.service.persistence.repository;

import com.heatmanofurioso.oss.cms.service.persistence.entity.ImageDocumentEntity;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ImageDocumentRepository extends JpaRepository<ImageDocumentEntity, Long> {

    ImageDocumentEntity findDocumentByDocumentGUID(@NonNull UUID documentGUID);

    List<ImageDocumentEntity> findAllByDocumentGUIDIn(@NonNull List<UUID> documentGUIDList);

    void deleteDocumentByDocumentGUID(@NonNull UUID guid);
}
