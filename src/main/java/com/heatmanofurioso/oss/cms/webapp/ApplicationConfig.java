package com.heatmanofurioso.oss.cms.webapp;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = {
    "com.heatmanofurioso.oss.cms.service.persistence.repository",
})
@EntityScan(basePackages =
    {"com.heatmanofurioso.oss.cms.service.persistence.entity"})
@Configuration
public class ApplicationConfig {
}
