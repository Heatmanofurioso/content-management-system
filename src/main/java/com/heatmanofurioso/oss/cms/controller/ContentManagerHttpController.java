package com.heatmanofurioso.oss.cms.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.heatmanofurioso.oss.cms.context.IDocumentFactory;
import com.heatmanofurioso.oss.cms.context.IDocumentFeeder;
import com.heatmanofurioso.oss.cms.utils.FileUtils;
import com.heatmanofurioso.oss.cms.utils.dto.entities.EntityTypes;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageDocumentDTO;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageType;
import com.heatmanofurioso.oss.cms.utils.exceptions.FileServiceException;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.mime.MimeTypeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/documents")
@Slf4j
public class ContentManagerHttpController {
    private final IDocumentFactory documentFactory;
    private final IDocumentFeeder documentFeeder;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public ContentManagerHttpController(IDocumentFactory documentFactory,
                                        IDocumentFeeder documentFeeder) {
        this.documentFactory = documentFactory;
        this.documentFeeder = documentFeeder;
    }

    @GetMapping(value = "/{documentGUIDList}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ImageDocumentDTO>> getDocumentFileJSON(@PathVariable String[] documentGUIDList) {

        try {
            List<UUID> uuidList = Arrays.stream(documentGUIDList).map(UUID::fromString).collect(Collectors.toList());
            List<ImageDocumentDTO> documentDTO = documentFactory.getDocumentByGUIDList(uuidList);
            return new ResponseEntity<>(documentDTO, HttpStatus.OK);
        } catch (FileServiceException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (IllegalArgumentException ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/{documentGUID}", consumes = MediaType.ALL_VALUE)
    public ResponseEntity<byte[]> getDocumentFileOctet(@PathVariable String documentGUID) {

        try {
            UUID uuid = UUID.fromString(documentGUID);
            ImageDocumentDTO document = documentFactory.getDocumentByGUID(uuid);
            byte[] documentContent = document.getContent().array();
            document.setContent(null);

            HttpHeaders headers = new HttpHeaders();
            String mediaTypeString = FileUtils.getFileMediaType(documentContent);
            MediaType mediaType = MediaType.valueOf(mediaTypeString);
            headers.setContentLength(documentContent.length);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            headers.add("content-disposition", "inline;filename=" + document.getName());
            headers.set("metadata", objectMapper.writeValueAsString(document));
            headers.setContentType(mediaType);

            log.info("Retrieved document from GUID: {}", documentGUID);

            return new ResponseEntity<>(documentContent, headers, HttpStatus.OK);
        } catch (FileServiceException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (InvalidMediaTypeException | JsonProcessingException ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IllegalArgumentException ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<List<ImageDocumentDTO>> getDocumentList() {

        List<ImageDocumentDTO> documentByGUID = documentFactory.listDocuments();
        log.info("Retrieved document list");
        return ResponseEntity.ok(documentByGUID);
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteDocument(
        @RequestParam("entityType") EntityTypes entityType,
        @RequestParam("entityValue") String entityValue,
        @RequestParam("imageType") ImageType imageType,
        @RequestParam("guid") String guidFilename) {
        documentFeeder.deleteImageDocument(entityType, entityValue, UUID.fromString(guidFilename.substring(0, guidFilename.lastIndexOf('.'))), imageType);
        return ResponseEntity.ok().build();
    }

    @PostMapping
    public ResponseEntity<String> uploadDocumentFile(
        @RequestParam("file") MultipartFile file,
        @RequestParam("entityType") EntityTypes entityType,
        @RequestParam("entityValue") String entityValue,
        @RequestParam("imageType") ImageType imageType,
        @RequestParam(value = "fileName", required = false) String fileName) {

        try {
            byte[] fileBytes = file.getBytes();
            UUID documentGUID = documentFeeder.createImageDocument(entityType, entityValue, Optional.ofNullable(fileName).orElse(file.getName()), fileBytes, imageType);
            log.info("Created document with GUID: {}", documentGUID.toString());
            return new ResponseEntity<>(documentGUID.toString(), HttpStatus.OK);
        } catch (IOException ex) {
            log.error("IOException creating document: {}", ex.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (MimeTypeException ex) {
            log.error("MimeTypeException creating document: {}", ex.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
