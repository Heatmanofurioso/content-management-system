package com.heatmanofurioso.oss.cms.utils.dto.entities;

public enum ImageType {
    BANNER,
    PROFILE
}
