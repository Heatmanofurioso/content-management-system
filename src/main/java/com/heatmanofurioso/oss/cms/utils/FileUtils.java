package com.heatmanofurioso.oss.cms.utils;

import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.mime.MimeTypeException;

public class FileUtils {
    private static final Tika tikaInstance = new Tika();

    public static String getFileMediaType(byte[] fileContent) {
        return tikaInstance.detect(fileContent);
    }

    public static String getFileExtension(byte[] fileContent, String originalFilename) throws MimeTypeException {
        TikaConfig config = TikaConfig.getDefaultConfig();

        String mediaType;
        try {
            mediaType = tikaInstance.detect(originalFilename);
        } catch (IllegalStateException ex) {
            mediaType = tikaInstance.detect(fileContent);
        }
        return config.getMimeRepository().forName(mediaType).getExtension();
    }
}
