package com.heatmanofurioso.oss.cms.utils.dto.entities;

public enum EntityTypes {
    USER,
    CONCERT,
    ARTIST,
    VENUE,
    FESTIVAL,
    HIGHLIGHT
}
