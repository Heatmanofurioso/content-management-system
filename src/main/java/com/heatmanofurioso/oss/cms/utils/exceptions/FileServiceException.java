package com.heatmanofurioso.oss.cms.utils.exceptions;

import java.io.IOException;

public class FileServiceException extends IOException {
    public FileServiceException(String message) {
        super(message);
    }

    public FileServiceException(IOException message) {
        super(message);
    }
}
