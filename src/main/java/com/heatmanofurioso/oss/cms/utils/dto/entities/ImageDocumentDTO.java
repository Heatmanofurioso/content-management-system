package com.heatmanofurioso.oss.cms.utils.dto.entities;

import lombok.Data;

import java.nio.ByteBuffer;

@Data
public class ImageDocumentDTO {

    private EntityTypes entityType;
    private String entityValue;
    private String documentGUID;
    private long version;
    private String name;
    private ByteBuffer content;
}
