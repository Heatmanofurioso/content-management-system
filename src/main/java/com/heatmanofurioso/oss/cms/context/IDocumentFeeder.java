package com.heatmanofurioso.oss.cms.context;

import com.heatmanofurioso.oss.cms.utils.dto.entities.EntityTypes;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageType;
import com.heatmanofurioso.oss.cms.utils.exceptions.FileServiceException;
import lombok.NonNull;
import org.apache.tika.mime.MimeTypeException;

import java.util.UUID;

public interface IDocumentFeeder {
    UUID createImageDocument(@NonNull EntityTypes entityType, @NonNull String entityValue, String originalFileName, byte[] fileBytes,
                             @NonNull ImageType imageType) throws MimeTypeException, FileServiceException;

    void deleteImageDocument(@NonNull EntityTypes entityType, @NonNull String entityValue, @NonNull UUID guid, @NonNull ImageType imageType);
}
