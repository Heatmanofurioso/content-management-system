package com.heatmanofurioso.oss.cms.context.mapper;

import com.heatmanofurioso.oss.cms.service.persistence.entity.ImageDocumentEntity;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageDocumentDTO;
import lombok.NonNull;

import java.nio.ByteBuffer;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DocumentMapper {

    public ImageDocumentDTO mapDocumentEntityToDocument(@NonNull ImageDocumentEntity input, byte[] documentMetadata) {
        ImageDocumentDTO output = new ImageDocumentDTO();

        output.setDocumentGUID(input.getDocumentGUID().toString());
        output.setVersion(input.getVersion());
        output.setEntityType(input.getEntityType());
        output.setEntityValue(input.getEntityValue());
        output.setName(Optional.ofNullable(input.getDocumentOriginalName())
                               .orElse(Paths.get(input.getDocumentPath()).getFileName().toString()));
        output.setContent(Optional.ofNullable(documentMetadata)
                                  .map(ByteBuffer::wrap).orElse(null));

        return output;
    }

    public List<ImageDocumentDTO> mapDocumentEntityListToDocumentList(List<ImageDocumentEntity> input) {
        List<ImageDocumentDTO> output = new ArrayList<>();

        input.forEach(eachInput -> output.add(mapDocumentEntityToDocument(eachInput, null)));

        return output;
    }
}
