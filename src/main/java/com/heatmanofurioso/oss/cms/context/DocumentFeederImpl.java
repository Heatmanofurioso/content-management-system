package com.heatmanofurioso.oss.cms.context;

import com.heatmanofurioso.oss.cms.service.persistence.IDocumentService;
import com.heatmanofurioso.oss.cms.service.persistence.IFileService;
import com.heatmanofurioso.oss.cms.service.persistence.entity.ImageDocumentEntity;
import com.heatmanofurioso.oss.cms.utils.FileUtils;
import com.heatmanofurioso.oss.cms.utils.dto.entities.EntityTypes;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageType;
import com.heatmanofurioso.oss.cms.utils.exceptions.FileServiceException;
import lombok.NonNull;
import org.apache.tika.mime.MimeTypeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class DocumentFeederImpl implements IDocumentFeeder {

    private final IDocumentService documentService;
    private final IFileService fileService;

    @Autowired
    public DocumentFeederImpl(IDocumentService documentService,
                              IFileService fileService) {
        this.documentService = documentService;
        this.fileService = fileService;
    }

    @Override
    public UUID createImageDocument(@NonNull EntityTypes entityType, @NonNull String entityValue, String originalFileName, byte[] fileBytes,
                                    ImageType imageType) throws MimeTypeException, FileServiceException {
        UUID documentGUID = UUID.randomUUID();
        String fileExtension = FileUtils.getFileExtension(fileBytes, originalFileName);
        String fileName = documentGUID.toString().concat(fileExtension);

        String documentPath = fileService.saveDocumentContent(entityType, entityValue, fileName, fileBytes, imageType);

        ImageDocumentEntity documentEntity = documentService.create(entityType, entityValue, documentGUID, documentPath, originalFileName, imageType);
        return documentEntity.getDocumentGUID();
    }

    @Override
    public void deleteImageDocument(@NonNull EntityTypes entityType, @NonNull String entityValue, @NonNull UUID guid, ImageType imageType) {
        ImageDocumentEntity byId = documentService.findById(guid);

        if (byId != null && byId.getEntityType().name().equalsIgnoreCase(entityType.name()) && byId.getEntityValue().equalsIgnoreCase(entityValue) && byId.getImageType().name().equalsIgnoreCase(
            imageType.name())) {
            fileService.deleteDocumentContent(byId.getDocumentPath());
            documentService.deleteById(guid);
        }
    }
}
