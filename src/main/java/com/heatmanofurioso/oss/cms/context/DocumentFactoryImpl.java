package com.heatmanofurioso.oss.cms.context;

import com.heatmanofurioso.oss.cms.context.mapper.DocumentMapper;
import com.heatmanofurioso.oss.cms.service.persistence.IDocumentService;
import com.heatmanofurioso.oss.cms.service.persistence.IFileService;
import com.heatmanofurioso.oss.cms.service.persistence.entity.ImageDocumentEntity;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageDocumentDTO;
import com.heatmanofurioso.oss.cms.utils.exceptions.FileServiceException;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class DocumentFactoryImpl implements IDocumentFactory {

    private final IDocumentService documentService;
    private final IFileService fileService;
    private final DocumentMapper documentMapper = new DocumentMapper();

    @Autowired
    public DocumentFactoryImpl(IDocumentService documentService,
                               IFileService fileService) {
        this.documentService = documentService;
        this.fileService = fileService;
    }

    @Override
    public ImageDocumentDTO getDocumentByGUID(@NonNull UUID uuid) throws FileServiceException {
        ImageDocumentEntity imageDocumentEntity = documentService.findById(uuid);

        if (imageDocumentEntity == null) {
            throw new FileServiceException(String.format("File %s not found", uuid));
        }
        byte[] document = fileService.getDocumentContent(imageDocumentEntity.getDocumentPath());

        return documentMapper.mapDocumentEntityToDocument(imageDocumentEntity, document);
    }

    @Override
    public List<ImageDocumentDTO> listDocuments() {
        List<ImageDocumentEntity> all = documentService.findAll();

        return documentMapper.mapDocumentEntityListToDocumentList(all);
    }

    @Override
    public List<ImageDocumentDTO> getDocumentByGUIDList(@NonNull List<UUID> uuidList) throws FileServiceException {
        List<ImageDocumentEntity> imageDocumentEntityList = documentService.findAllByIdList(uuidList);
        List<ImageDocumentDTO> imageDocumentDTOList = new ArrayList<>();

        for (ImageDocumentEntity imageDocumentEntity : imageDocumentEntityList) {
            byte[] document = fileService.getDocumentContent(imageDocumentEntity.getDocumentPath());

            ImageDocumentDTO imageDocumentDTO = documentMapper.mapDocumentEntityToDocument(imageDocumentEntity, document);
            imageDocumentDTOList.add(imageDocumentDTO);
        }
        return imageDocumentDTOList;
    }
}
