package com.heatmanofurioso.oss.cms.context;

import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageDocumentDTO;
import com.heatmanofurioso.oss.cms.utils.exceptions.FileServiceException;
import lombok.NonNull;

import java.util.List;
import java.util.UUID;

public interface IDocumentFactory {

    ImageDocumentDTO getDocumentByGUID(@NonNull UUID uuid) throws FileServiceException;

    List<ImageDocumentDTO> listDocuments();

    List<ImageDocumentDTO> getDocumentByGUIDList(@NonNull List<UUID> uuidList) throws FileServiceException;
}
