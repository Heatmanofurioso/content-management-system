package com.heatmanofurioso.oss.cms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(
    scanBasePackages = {
        "com.heatmanofurioso.oss.cms"
    })
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
