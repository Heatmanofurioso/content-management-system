package com.heatmanofurioso.oss.cms.webapp;

import com.heatmanofurioso.oss.cms.context.IDocumentFactory;
import com.heatmanofurioso.oss.cms.context.IDocumentFeeder;
import com.heatmanofurioso.oss.cms.utils.dto.entities.EntityTypes;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageDocumentDTO;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageType;
import com.heatmanofurioso.oss.cms.utils.exceptions.FileServiceException;
import org.apache.tika.mime.MimeTypeException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
@TestPropertySource(properties = {"spring.application.name=applicationName"})
class ContentManagerHttpControllerUnitTest {

    @MockBean
    private IDocumentFactory documentFactory;

    @MockBean
    private IDocumentFeeder documentFeeder;

    @Autowired
    private MockMvc mvc;

    private ImageDocumentDTO createDocument(UUID uuid) throws IOException {
        ImageDocumentDTO document = new ImageDocumentDTO();
        document.setDocumentGUID(uuid.toString());
        byte[] bytes = Files.readAllBytes(Paths.get("src/test/resources/file.txt"));
        document.setContent(ByteBuffer.wrap(bytes));
        document.setName("file.txt");
        document.setVersion(1);
        return document;
    }

    @Test
    @DisplayName("""
        Given a document controller
        When a call for listing documents is made
        Then the list of available documents is returned
        """)
    void testGetDocumentList() throws Exception {
        ImageDocumentDTO document = createDocument(UUID.randomUUID());
        when(documentFactory.listDocuments()).thenReturn(Collections.singletonList(document));

        mvc.perform(MockMvcRequestBuilders.get("/documents"))
           .andExpect(status().isOk());
    }

    @Test
    void testGetDocumentJSONHappyPath() throws Exception {
        ImageDocumentDTO document = createDocument(UUID.randomUUID());
        when(documentFactory.getDocumentByGUID(any(UUID.class))).thenReturn(document);

        mvc.perform(MockMvcRequestBuilders.get("/documents/".concat(document.getDocumentGUID())).contentType("application/json"))
           .andExpect(status().isOk());
    }

    @Test
    void testGetDocumentJSONWrongGUID() throws Exception {
        ImageDocumentDTO document = createDocument(UUID.randomUUID());
        when(documentFactory.getDocumentByGUID(any(UUID.class))).thenReturn(document);

        mvc.perform(MockMvcRequestBuilders.get("/documents/banana").contentType("application/json"))
           .andExpect(status().isBadRequest());
    }

    @Test
    void testGetDocumentJSONNotFound() throws Exception {
        when(documentFactory.getDocumentByGUID(any(UUID.class))).thenThrow(new FileServiceException("File does not exist"));

        UUID uuid = UUID.randomUUID();
        mvc.perform(MockMvcRequestBuilders.get("/documents/".concat(uuid.toString())).contentType("application/json"))
           .andExpect(status().isOk());
    }

    @Test
    void testGetDocumentBytesHappyPath() throws Exception {
        ImageDocumentDTO document = createDocument(UUID.randomUUID());
        when(documentFactory.getDocumentByGUID(any(UUID.class))).thenReturn(document);

        mvc.perform(MockMvcRequestBuilders.get("/documents/".concat(document.getDocumentGUID())))
           .andExpect(status().isOk())
           .andExpect(content().contentType("text/plain"));
    }

    @Test
    void testDeleteDocumentBadRequest() throws Exception {

        mvc.perform((MockMvcRequestBuilders.delete("/documents/")))
           .andExpect(status().isBadRequest());
    }

    @Test
    void testDeleteDocumentHappyPath() throws Exception {
        doNothing().when(documentFeeder).deleteImageDocument(any(), any(), any(), any(ImageType.class));

        mvc.perform((MockMvcRequestBuilders.delete("/documents/")
                                           .queryParam("entityType", "ARTIST")
                                           .queryParam("entityValue", "100")
                                           .queryParam("imageType", "BANNER")
                                           .queryParam("guid", UUID.randomUUID() + ".jpg")))
           .andExpect(status().isOk());
    }

    @Test
    void testGetDocumentBytesWrongGUID() throws Exception {
        ImageDocumentDTO document = createDocument(UUID.randomUUID());
        when(documentFactory.getDocumentByGUID(any(UUID.class))).thenReturn(document);

        mvc.perform(MockMvcRequestBuilders.get("/documents/banana"))
           .andExpect(status().isBadRequest());
    }

    @Test
    void testGetDocumentBytesNotFound() throws Exception {
        when(documentFactory.getDocumentByGUID(any(UUID.class))).thenThrow(new FileServiceException("File does not exist"));

        UUID uuid = UUID.randomUUID();
        mvc.perform(MockMvcRequestBuilders.get("/documents/".concat(uuid.toString())))
           .andExpect(status().isNotFound());
    }

    @Test
    void testSendDocumentHappyPath() throws Exception {
        UUID uuid = UUID.randomUUID();
        when(documentFeeder.createImageDocument(any(EntityTypes.class), anyString(), anyString(), any(), any(ImageType.class))).thenReturn(uuid);

        MockMultipartFile file = new MockMultipartFile("file", "file.txt", "text/plain", "test file".getBytes());

        mvc.perform(MockMvcRequestBuilders.multipart("/documents")
                                          .file(file)
                                          .param("entityType", "ARTIST")
                                          .param("imageType", "PROFILE")
                                          .param("entityValue", "ARTIST")
                                          .param("fileName", "file.txt"))
           .andExpect(status().isOk());
    }

    @Test
    void testSendDocumentNoCaseId() throws Exception {
        UUID uuid = UUID.randomUUID();
        when(documentFeeder.createImageDocument(any(EntityTypes.class), anyString(), anyString(), any(), any(ImageType.class))).thenReturn(uuid);

        MockMultipartFile file = new MockMultipartFile("file", "file.txt", "text/plain", "test file".getBytes());

        mvc.perform(MockMvcRequestBuilders.multipart("/documents")
                                          .file(file)
                                          .param("fileName", "file.txt"))
           .andExpect(status().isBadRequest());
    }

    @Test
    void testSendDocumentIOException() throws Exception {
        when(documentFeeder.createImageDocument(any(EntityTypes.class), anyString(), anyString(), any(), any(ImageType.class))).thenThrow(new FileServiceException("Error"));

        MockMultipartFile file = new MockMultipartFile("file", "file.txt", "text/plain", "test file".getBytes());

        mvc.perform(MockMvcRequestBuilders.multipart("/documents")
                                          .file(file)
                                          .param("entityType", "ARTIST")
                                          .param("fileName", "file.txt"))
           .andExpect(status().isBadRequest());
    }

    @Test
    void testSendDocumentMimeException() throws Exception {
        when(documentFeeder.createImageDocument(any(EntityTypes.class), anyString(), anyString(), any(), any(ImageType.class))).thenThrow(new MimeTypeException("Error"));

        MockMultipartFile file = new MockMultipartFile("file", "file.txt", "text/plain", "test file".getBytes());

        mvc.perform(MockMvcRequestBuilders.multipart("/documents")
                                          .file(file)
                                          .param("entityType", "ARTIST")
                                          .param("fileName", "file.txt"))
           .andExpect(status().isBadRequest());
    }
}