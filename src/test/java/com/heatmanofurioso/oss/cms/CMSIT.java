package com.heatmanofurioso.oss.cms;

import com.heatmanofurioso.oss.cms.service.persistence.entity.ImageDocumentEntity;
import com.heatmanofurioso.oss.cms.service.persistence.repository.ImageDocumentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CMSIT extends AbstractIntegrationTest {

    @Autowired
    private ImageDocumentRepository repository;

    @Test
    void testRepositoryConnection() {

        assertNotNull(repository);
        List<ImageDocumentEntity> all = repository.findAll();
        assertNotNull(all);
        assertEquals(0, all.size());
    }
}
