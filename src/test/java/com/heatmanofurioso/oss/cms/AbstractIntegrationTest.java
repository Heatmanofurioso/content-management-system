package com.heatmanofurioso.oss.cms;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.lifecycle.Startables;
import org.testcontainers.utility.DockerImageName;

import java.util.stream.Stream;

@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@ActiveProfiles("test")
@DirtiesContext
public abstract class AbstractIntegrationTest {

    @Container
    public static final MySQLContainer<?> mysqldb = new MySQLContainer<>(DockerImageName.parse("mysql:8.0.27")).withDatabaseName("CMS").withUsername("user").withPassword("pass");

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        Startables.deepStart(Stream.of(mysqldb)).join();
        registry.add("spring.datasource.url=", mysqldb::getJdbcUrl);
    }
}
