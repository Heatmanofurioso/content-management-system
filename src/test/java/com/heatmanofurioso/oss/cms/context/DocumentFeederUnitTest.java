package com.heatmanofurioso.oss.cms.context;

import com.heatmanofurioso.oss.cms.service.persistence.IDocumentService;
import com.heatmanofurioso.oss.cms.service.persistence.IFileService;
import com.heatmanofurioso.oss.cms.service.persistence.entity.ImageDocumentEntity;
import com.heatmanofurioso.oss.cms.utils.dto.entities.EntityTypes;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageType;
import com.heatmanofurioso.oss.cms.utils.exceptions.FileServiceException;
import org.apache.tika.mime.MimeTypeException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.endsWith;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class DocumentFeederUnitTest {
    private static final String RESOURCES_PREFIX = "src/test/resources";
    private IDocumentFeeder documentFeeder;
    private IFileService fileService;
    private IDocumentService documentService;

    private ImageDocumentEntity createMockEntity() {
        ImageDocumentEntity documentEntity = new ImageDocumentEntity();
        documentEntity.setDocumentGUID(UUID.fromString("eb445c1e-83e8-11ea-b23d-369d12a75bed"));
        documentEntity.setImageType(ImageType.PROFILE);
        documentEntity.setId(1L);
        documentEntity.setDocumentPath("");
        documentEntity.setEntityValue("100");
        documentEntity.setEntityType(EntityTypes.ARTIST);
        return documentEntity;
    }

    private byte[] getFileBytes(String filename) throws IOException {
        return Files.readAllBytes(Paths.get(RESOURCES_PREFIX, filename));
    }

    @BeforeEach
    public void setup() throws FileServiceException {
        documentService = mock(IDocumentService.class);
        when(documentService.create(any(), anyString(), any(UUID.class), anyString(), anyString(), any())).thenReturn(createMockEntity());

        when(documentService.findById(any(UUID.class))).thenReturn(createMockEntity());

        fileService = mock(IFileService.class);
        when(fileService.saveDocumentContent(any(), anyString(), anyString(), any(), any(ImageType.class))).thenReturn("path/to/testFile.txt");

        documentFeeder = new DocumentFeederImpl(documentService, fileService);
    }

    @Test
    void testNullId() {
        assertThrows(NullPointerException.class, () -> documentFeeder.createImageDocument(null, null, null, null, null));
    }

    @Test
    void testHappyPathTxt() throws IOException, MimeTypeException {
        byte[] bytes = getFileBytes("textFile.txt");
        UUID uuid = documentFeeder.createImageDocument(EntityTypes.ARTIST, "100", "testFile.txt", bytes, ImageType.PROFILE);
        assertEquals("eb445c1e-83e8-11ea-b23d-369d12a75bed", uuid.toString());
        verify(fileService, times(1))
            .saveDocumentContent(eq(EntityTypes.ARTIST), eq("100"), endsWith(".txt"), eq(bytes), eq(ImageType.PROFILE));
        verify(documentService, times(1))
            .create(eq(EntityTypes.ARTIST), eq("100"), any(UUID.class), eq("path/to/testFile.txt"), eq("testFile.txt"), eq(ImageType.PROFILE));
    }

    @Test
    void testHappyPathJpg() throws IOException, MimeTypeException {
        byte[] bytes = getFileBytes("jpgFile.jpg");
        UUID uuid = documentFeeder.createImageDocument(EntityTypes.ARTIST, "100", "testFile.txt", bytes, ImageType.PROFILE);
        assertEquals("eb445c1e-83e8-11ea-b23d-369d12a75bed", uuid.toString());
        verify(fileService, times(1))
            .saveDocumentContent(eq(EntityTypes.ARTIST), eq("100"), endsWith(".txt"), eq(bytes), eq(ImageType.PROFILE));
        verify(documentService, times(1))
            .create(eq(EntityTypes.ARTIST), eq("100"), any(UUID.class), eq("path/to/testFile.txt"), eq("testFile.txt"), eq(ImageType.PROFILE));
    }

    @Test
    void testDeleteImageDocument() {

        UUID uuid = UUID.randomUUID();

        documentFeeder.deleteImageDocument(EntityTypes.ARTIST, "100", uuid, ImageType.PROFILE);

        verify(fileService, times(1))
            .deleteDocumentContent(any(String.class));
        verify(documentService, times(1))
            .deleteById(uuid);
    }
}
