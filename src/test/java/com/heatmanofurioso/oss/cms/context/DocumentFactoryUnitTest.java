package com.heatmanofurioso.oss.cms.context;

import com.heatmanofurioso.oss.cms.service.persistence.IDocumentService;
import com.heatmanofurioso.oss.cms.service.persistence.IFileService;
import com.heatmanofurioso.oss.cms.service.persistence.entity.ImageDocumentEntity;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageDocumentDTO;
import com.heatmanofurioso.oss.cms.utils.exceptions.FileServiceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class DocumentFactoryUnitTest {

    private IDocumentFactory documentFactory;
    private IFileService fileService;
    private IDocumentService documentService;
    private final UUID existingUUID = UUID.fromString("eb445c1e-83e8-11ea-b23d-369d12a75bed");

    private ImageDocumentEntity createDocumentEntity(UUID uuid) {
        ImageDocumentEntity entity = new ImageDocumentEntity();
        entity.setDocumentGUID(uuid);
        entity.setDocumentPath("path" + File.separator + "to" + File.separator + "document.txt");
        entity.setDocumentOriginalName("pdfFile.pdf");
        entity.setId(1L);
        entity.setVersion(1L);
        return entity;
    }

    @BeforeEach
    public void setup() throws FileServiceException {
        documentService = mock(IDocumentService.class);
        when(documentService.findById(any(UUID.class))).thenAnswer(invocation -> {
            UUID uuid = invocation.getArgument(0, UUID.class);
            if (uuid.equals(existingUUID)) {
                return createDocumentEntity(existingUUID);
            } else {
                return null;
            }
        });
        when(documentService.findAll()).thenReturn(Collections.singletonList(createDocumentEntity(existingUUID)));

        when(documentService.findAllByIdList(anyList())).thenReturn(Collections.singletonList(createDocumentEntity(existingUUID)));

        fileService = mock(IFileService.class);
        when(fileService.getDocumentContent(any(String.class))).thenReturn(new byte[0]);

        documentFactory = new DocumentFactoryImpl(documentService, fileService);
    }

    @Test
    void testNonExistingDocument() throws FileServiceException {
        UUID uuid = UUID.randomUUID();
        FileServiceException ex = assertThrows(FileServiceException.class, () -> documentFactory.getDocumentByGUID(uuid));
        assertEquals(ex.getMessage(), "File ".concat(uuid.toString().concat(" not found")));

        verify(documentService, times(1))
            .findById(uuid);
        verify(fileService, times(0))
            .getDocumentContent(anyString());
    }

    @Test
    void testAllDocuments() {
        List<ImageDocumentDTO> imageDocumentDTOS = documentFactory.listDocuments();
        assertNotNull(documentFactory);
        assertEquals(1, imageDocumentDTOS.size());
        assertNull(imageDocumentDTOS.get(0).getContent());
    }

    @Test
    void testGetDocumentByGUIDList() throws FileServiceException {
        List<ImageDocumentDTO> documentByGUIDList = documentFactory.getDocumentByGUIDList(Collections.singletonList(existingUUID));

        assertNotNull(documentByGUIDList);
        assertEquals(1, documentByGUIDList.size());
    }
}
