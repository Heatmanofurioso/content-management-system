package com.heatmanofurioso.oss.cms.context;

import com.heatmanofurioso.oss.cms.context.mapper.DocumentMapper;
import com.heatmanofurioso.oss.cms.service.persistence.entity.ImageDocumentEntity;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageDocumentDTO;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MappingUnitTest {
    private final DocumentMapper mapper = new DocumentMapper();

    private ImageDocumentEntity createImageDocumentEntity(UUID uuid) {
        ImageDocumentEntity entity = new ImageDocumentEntity();
        entity.setDocumentGUID(uuid);
        entity.setDocumentPath("path/to/document.txt");
        entity.setDocumentOriginalName("pdfFile.pdf");
        entity.setId(1L);
        entity.setVersion(1L);
        return entity;
    }

    @Test
    void mapNullEntity() {
        assertThrows(NullPointerException.class, () -> mapper.mapDocumentEntityToDocument(null, null));
    }

    @Test
    void mapCompleteDocument() {
        UUID uuid = UUID.randomUUID();
        ImageDocumentEntity documentEntity = createImageDocumentEntity(uuid);
        ImageDocumentDTO document = mapper.mapDocumentEntityToDocument(documentEntity, new byte[0]);
        assertNotNull(document);
        assertEquals(document.getDocumentGUID(), documentEntity.getDocumentGUID().toString());
        assertEquals(document.getVersion(), documentEntity.getVersion().longValue());
        assertEquals(document.getName(), documentEntity.getDocumentOriginalName());
        assertArrayEquals(new byte[0], document.getContent().array());
    }

    @Test
    void mapDocumentWithoutName() {
        UUID uuid = UUID.randomUUID();
        ImageDocumentEntity documentEntity = createImageDocumentEntity(uuid);
        documentEntity.setDocumentOriginalName(null);
        documentEntity.setDocumentPath("/path/to/document.pdf");
        ImageDocumentDTO document = mapper.mapDocumentEntityToDocument(documentEntity, new byte[0]);
        assertNotNull(document);
        assertEquals("document.pdf", document.getName());
    }

    @Test
    void mapDocumentWithoutContent() {
        UUID uuid = UUID.randomUUID();
        ImageDocumentEntity documentEntity = createImageDocumentEntity(uuid);
        ImageDocumentDTO document = mapper.mapDocumentEntityToDocument(documentEntity, null);
        assertNotNull(document);
        assertNull(document.getContent());
    }
}
