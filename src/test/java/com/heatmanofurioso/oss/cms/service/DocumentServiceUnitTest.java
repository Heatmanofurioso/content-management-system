package com.heatmanofurioso.oss.cms.service;

import com.heatmanofurioso.oss.cms.service.persistence.DocumentServiceImpl;
import com.heatmanofurioso.oss.cms.service.persistence.IDocumentService;
import com.heatmanofurioso.oss.cms.service.persistence.entity.ImageDocumentEntity;
import com.heatmanofurioso.oss.cms.service.persistence.repository.ImageDocumentRepository;
import com.heatmanofurioso.oss.cms.utils.dto.entities.EntityTypes;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class DocumentServiceUnitTest {
    private final UUID existingUUID = UUID.fromString("f49452c9-21ef-42f2-9b9b-28a03b4e2f95");
    private ImageDocumentRepository documentRepository;
    private IDocumentService documentService;

    private ImageDocumentEntity createDocumentEntity(UUID uuid) {
        return new ImageDocumentEntity(1L, uuid, "path/file.txt", ImageType.PROFILE, "file.txt", EntityTypes.ARTIST, "100", 1L);
    }

    private ImageDocumentEntity getDocumentMock(UUID argument) {
        if (argument.toString().equals(existingUUID.toString())) {
            return createDocumentEntity(existingUUID);
        } else {
            return null;
        }
    }

    @BeforeEach
    public void setup() {
        documentRepository = mock(ImageDocumentRepository.class);
        when(documentRepository.findAll()).thenReturn(Collections.singletonList(createDocumentEntity(UUID.randomUUID())));

        when(documentRepository.findDocumentByDocumentGUID(any(UUID.class)))
            .thenAnswer(invocation -> getDocumentMock(invocation.getArgument(0)));

        when(documentRepository.save(any(ImageDocumentEntity.class)))
            .thenAnswer(invocation -> invocation.getArgument(0));

        documentService = new DocumentServiceImpl(documentRepository);
    }

    @Test
    void testFindAll() {
        List<ImageDocumentEntity> documents = documentService.findAll();
        assertNotNull(documents);
        assertEquals(1, documents.size());
    }

    @Test
    void testFindByGUID() {
        assertNull(documentService.findById(UUID.fromString("aa569a39-178d-48b0-88a7-23a37f2990c9")));
        assertNotNull(documentService.findById(existingUUID));
    }

    @Test
    void testCreate() {
        UUID uuid = UUID.randomUUID();
        ImageDocumentEntity entity = documentService.create(
            EntityTypes.ARTIST, "", uuid, "path/file.txt", "file.txt", ImageType.PROFILE);
        assertNotNull(entity);
        assertEquals(uuid, entity.getDocumentGUID());
        assertNull(entity.getId());
        assertEquals("path/file.txt", entity.getDocumentPath());
        assertEquals("file.txt", entity.getDocumentOriginalName());
        verify(documentRepository, times(1))
            .save(entity);
        if (true) {
            System.out.print("First");
        } else {
            System.out.print("Second");
        }
    }

    @Test
    void testFindAllByIdList() {
        List<ImageDocumentEntity> allByIdList = documentService.findAllByIdList(new ArrayList<>());

        assertNotNull(allByIdList);
        assertEquals(0, allByIdList.size());
    }

    @Test
    void testDeleteById() {
        documentService.deleteById(existingUUID);
        verify(documentRepository, times(1)).deleteDocumentByDocumentGUID(existingUUID);
    }
}
