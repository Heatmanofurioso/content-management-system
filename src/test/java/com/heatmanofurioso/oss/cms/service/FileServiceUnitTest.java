package com.heatmanofurioso.oss.cms.service;

import com.heatmanofurioso.oss.cms.service.persistence.fileservice.FileServiceImpl;
import com.heatmanofurioso.oss.cms.service.persistence.IFileService;
import com.heatmanofurioso.oss.cms.utils.dto.entities.EntityTypes;
import com.heatmanofurioso.oss.cms.utils.dto.entities.ImageType;
import com.heatmanofurioso.oss.cms.utils.exceptions.FileServiceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FileServiceUnitTest {
    private final String TEST_OUTPUT_FOLDER = "src/test/resources";
    private IFileService fileService;

    @BeforeEach
    public void setup() {
        fileService = new FileServiceImpl(TEST_OUTPUT_FOLDER);
    }

    @Test
    void testNullInputs() {
        assertThrows(NullPointerException.class, () -> fileService.getDocumentContent(null));
        assertThrows(NullPointerException.class, () -> fileService.saveDocumentContent(null, null, null, null, null));
        assertThrows(NullPointerException.class, () -> fileService.saveDocumentContent(EntityTypes.ARTIST, "100", null, null, null));
        assertThrows(NullPointerException.class, () -> fileService.saveDocumentContent(EntityTypes.ARTIST, "100", "testFile.txt", null, null));
    }

    @Test
    void getDocumentThatDoesNotExistThrows() {
        FileServiceException ex = assertThrows(FileServiceException.class, () -> fileService.getDocumentContent("path/to/testFile.txt"));
        assertEquals("File does not exist or cannot be opened: path/to/testFile.txt", ex.getMessage());
    }

    @Test
    void getDocumentThatExists() throws IOException {
        byte[] bytes = fileService.getDocumentContent("test-folder" + File.separator + "testFile.txt");
        assertNotNull(bytes);
    }

    @Test
    void testHappyPath() throws IOException {
        byte[] bytes = Files.readAllBytes(Paths.get(TEST_OUTPUT_FOLDER, "test-folder", "testFile.txt"));
        String documentPath = fileService.saveDocumentContent(EntityTypes.ARTIST, "200", "testFile.txt", bytes, ImageType.PROFILE);
        assertEquals("200" + File.separator + ImageType.PROFILE.name() + File.separator + "testFile.txt", Paths.get("200", ImageType.PROFILE.name(), "testFile.txt").toString());
        assertTrue(Paths.get(TEST_OUTPUT_FOLDER, documentPath).toFile().exists());
        Files.delete(Paths.get(TEST_OUTPUT_FOLDER, ImageType.PROFILE.name() + File.separator + "ARTIST" + File.separator + "200" + File.separator + "testFile.txt"));
        Files.delete(Paths.get(TEST_OUTPUT_FOLDER, ImageType.PROFILE.name() + File.separator + "ARTIST" + File.separator + "200"));
        Files.delete(Paths.get(TEST_OUTPUT_FOLDER, ImageType.PROFILE.name() + File.separator + "ARTIST"));
        Files.delete(Paths.get(TEST_OUTPUT_FOLDER, ImageType.PROFILE.name()));
    }
}
